#global candidate rc2

%if 0%{?fedora}
%bcond_without mingw

# uses wine, requires enabled binfmt
%bcond_with tests
%else
%bcond_with mingw
%endif

Name:     opus
Version:  1.4
Release:  6%{?candidate:.%{candidate}}%{?dist}
Summary:  An audio codec for use in low-delay speech and audio communication
License:  BSD-3-Clause AND BSD-2-Clause
URL:      https://www.opus-codec.org/

Source0:  https://ftp.osuosl.org/pub/xiph/releases/%{name}/%{name}-%{version}%{?candidate:-%{candidate}}.tar.gz
# This is the final IETF Working Group RFC
Source1:  https://tools.ietf.org/rfc/rfc6716.txt
Source2:  https://tools.ietf.org/rfc/rfc8251.txt

BuildRequires: make
BuildRequires: gcc
BuildRequires: doxygen
BuildRequires: libtool

%if %{with mingw}
BuildRequires: mingw32-filesystem
BuildRequires: mingw32-gcc

BuildRequires: mingw64-gcc
BuildRequires: mingw64-filesystem

%if %{with tests}
BuildRequires: wine
%endif
%endif

%description
The Opus codec is designed for interactive speech and audio transmission over
the Internet. It is designed by the IETF Codec Working Group and incorporates
technology from Skype's SILK codec and Xiph.Org's CELT codec.

%package  devel
Summary:  Development package for opus
Requires: libogg-devel
Requires: opus = %{version}-%{release}

%description devel
Files for development with opus.

%if %{with mingw}
%package -n mingw32-%{name}
Summary: MinGW compiled %{name} library for Win32 target
BuildArch: noarch

%description -n mingw32-%{name}
This package contains the MinGW compiled library of %{name}
for Win32 target.

%package -n mingw64-%{name}
Summary: MinGW compiled %{name} library for Win64 target
BuildArch: noarch

%description -n mingw64-%{name}
This package contains the MinGW compiled library of %{name}
for Win64 target.

%{?mingw_debug_package}
%endif

%prep
%setup -q %{?candidate:-n %{name}-%{version}-%{candidate}}
cp %{SOURCE1} .
cp %{SOURCE2} .

%build
autoreconf -ivf
mkdir build_native
pushd build_native
%global _configure ../configure
%configure --enable-custom-modes --disable-static \
           --enable-hardening \
%ifarch %{arm} %{arm64} %{power64}
        --enable-fixed-point
%endif

%make_build
popd

%if %{with mingw}
%mingw_configure --enable-custom-modes --disable-static --disable-doc
%mingw_make %{?_smp_mflags} V=1
%endif

%install
%make_install -C build_native

rm %{buildroot}%{_libdir}/libopus.la
rm -rf %{buildroot}%{_datadir}/doc/opus

%if %{with mingw}
%mingw_make_install DESTDIR=%{buildroot}
rm %{buildroot}%{mingw32_libdir}/libopus.la
rm %{buildroot}%{mingw64_libdir}/libopus.la
%mingw_debug_install_post
%endif

%check
make -C build_native check %{?_smp_mflags} V=1

%ldconfig_scriptlets

%if %{with mingw}
%if %{with tests}
%mingw_make check
%endif
%endif

%files
%license COPYING
%{_libdir}/libopus.so.*

%files devel
%doc README build_native/doc/html rfc6716.txt rfc8251.txt
%{_includedir}/opus
%{_libdir}/libopus.so
%{_libdir}/pkgconfig/opus.pc
%{_datadir}/aclocal/opus.m4
%{_datadir}/man/man3/opus_*.3.gz

%if %{with mingw}
%files -n mingw32-%{name}
%license COPYING
%dir %{mingw32_includedir}/opus/
%{mingw32_bindir}/libopus-0.dll
%{mingw32_includedir}/opus/*.h
%{mingw32_libdir}/libopus.dll.a
%{mingw32_libdir}/pkgconfig/opus.pc
%{mingw32_datadir}/aclocal/opus.m4

%files -n mingw64-%{name}
%license COPYING
%dir %{mingw64_includedir}/opus/
%{mingw64_bindir}/libopus-0.dll
%{mingw64_includedir}/opus/*.h
%{mingw64_libdir}/libopus.dll.a
%{mingw64_libdir}/pkgconfig/opus.pc
%{mingw64_datadir}/aclocal/opus.m4
%endif

%changelog
* Tue Oct 29 2024 Troy Dawson <tdawson@redhat.com> - 1.4-6
- Bump release for October 2024 mass rebuild:
  Resolves: RHEL-64018

* Mon Jun 24 2024 Troy Dawson <tdawson@redhat.com> - 1.4-5
- Bump release for June 2024 mass rebuild

* Thu Jan 25 2024 Fedora Release Engineering <releng@fedoraproject.org> - 1.4-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_40_Mass_Rebuild

* Sun Jan 21 2024 Fedora Release Engineering <releng@fedoraproject.org> - 1.4-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_40_Mass_Rebuild

* Wed Nov 08 2023 Marian Koncek <mkoncek@redhat.com> - 1.4-2
- Make mingw subpackages noarch

* Wed Sep 27 2023 Marian Koncek <mkoncek@redhat.com> - 1.4-1
- Update to upstream version 1.4
- Add mingw subpackages

* Thu Jul 20 2023 Fedora Release Engineering <releng@fedoraproject.org> - 1.3.1-13
- Rebuilt for https://fedoraproject.org/wiki/Fedora_39_Mass_Rebuild

* Thu Jan 19 2023 Fedora Release Engineering <releng@fedoraproject.org> - 1.3.1-12
- Rebuilt for https://fedoraproject.org/wiki/Fedora_38_Mass_Rebuild
- Fix build in all arches, copied from Debian
- Run autoreconf to make sure that autotools are updated
- Clean up some trailing white spaces

* Fri Jul 22 2022 Fedora Release Engineering <releng@fedoraproject.org> - 1.3.1-11
- Rebuilt for https://fedoraproject.org/wiki/Fedora_37_Mass_Rebuild

* Thu Jan 20 2022 Fedora Release Engineering <releng@fedoraproject.org> - 1.3.1-10
- Rebuilt for https://fedoraproject.org/wiki/Fedora_36_Mass_Rebuild

* Thu Jul 22 2021 Fedora Release Engineering <releng@fedoraproject.org> - 1.3.1-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_35_Mass_Rebuild

* Tue Jan 26 2021 Fedora Release Engineering <releng@fedoraproject.org> - 1.3.1-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_34_Mass_Rebuild

* Mon Sep 28 2020 Jeff Law <law@redhat.com> - 1.3.1-7
- Re-enable LTO as upstream GCC target/96939 has been fixed

* Mon Aug 10 2020 Jeff Law <law@redhat.com> - 1.3.1-6
- Disable LTO for now

* Sat Aug 01 2020 Fedora Release Engineering <releng@fedoraproject.org> - 1.3.1-5
- Second attempt - Rebuilt for
  https://fedoraproject.org/wiki/Fedora_33_Mass_Rebuild

* Tue Jul 28 2020 Fedora Release Engineering <releng@fedoraproject.org> - 1.3.1-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_33_Mass_Rebuild

* Wed Jan 29 2020 Fedora Release Engineering <releng@fedoraproject.org> - 1.3.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_32_Mass_Rebuild

* Thu Jul 25 2019 Fedora Release Engineering <releng@fedoraproject.org> - 1.3.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_31_Mass_Rebuild

* Sun Apr 14 2019 Peter Robinson <pbrobinson@fedoraproject.org> 1.3.1-1
- Update to 1.3.1

* Fri Feb 01 2019 Fedora Release Engineering <releng@fedoraproject.org> - 1.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_30_Mass_Rebuild

* Thu Oct 18 2018 Peter Robinson <pbrobinson@fedoraproject.org> 1.3-1
- Update to 1.3

* Wed Sep 19 2018 Peter Robinson <pbrobinson@fedoraproject.org> 1.3-0.7.rc2
- Update to 1.3 rc2

* Fri Jul 13 2018 Fedora Release Engineering <releng@fedoraproject.org> - 1.3-0.6.rc
- Rebuilt for https://fedoraproject.org/wiki/Fedora_29_Mass_Rebuild

* Sat Jun  2 2018 Peter Robinson <pbrobinson@fedoraproject.org> 1.3-0.5.rc
- Update to 1.3 rc

* Fri Mar  9 2018 Peter Robinson <pbrobinson@fedoraproject.org> 1.3-0.4.beta
- Add gcc BR

* Thu Feb 08 2018 Fedora Release Engineering <releng@fedoraproject.org> - 1.3-0.3.beta
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Sat Feb 03 2018 Igor Gnatenko <ignatenkobrain@fedoraproject.org> - 1.3-0.2.beta
- Switch to %%ldconfig_scriptlets

* Fri Dec 22 2017 Peter Robinson <pbrobinson@fedoraproject.org> 1.3-0.1.beta
- Update to 1.3 beta

* Thu Aug 03 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.2.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.2.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Wed Jun 28 2017 Peter Robinson <pbrobinson@fedoraproject.org> 1.2.1-1
- Update to 1.2.1

* Tue Jun 20 2017 Peter Robinson <pbrobinson@fedoraproject.org> 1.2-1
- Update to 1.2

* Fri Jun  9 2017 Peter Robinson <pbrobinson@fedoraproject.org> 1.2-0.4
- Update to 1.2.0 RC1

* Wed May 24 2017 Peter Robinson <pbrobinson@fedoraproject.org> 1.2-0.3
- Update to 1.2.0 Beta

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.2-0.2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Fri Nov  4 2016 Peter Robinson <pbrobinson@fedoraproject.org> 1.2-0.1
- Update to 1.2.0 Alpha

* Mon Jul 18 2016 Peter Robinson <pbrobinson@fedoraproject.org> 1.1.3-1
- Update 1.1.3 GA

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 1.1.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Tue Jan 12 2016 Peter Robinson <pbrobinson@fedoraproject.org> 1.1.2-1
- Update 1.1.2 GA

* Thu Nov 26 2015 Peter Robinson <pbrobinson@fedoraproject.org> 1.1.1-1
- Update 1.1.1 GA

* Wed Oct 28 2015 Peter Robinson <pbrobinson@fedoraproject.org> 1.1.1-0.4.rc
- Update to 1.1.1 RC (further ARM optimisations)

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.1.1-0.3.beta
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Tue Feb  3 2015 Peter Robinson <pbrobinson@fedoraproject.org> 1.1.1-0.2.beta
- Use %%license

* Wed Oct 15 2014 Peter Robinson <pbrobinson@fedoraproject.org> 1.1.1-0.1.beta
- Update to 1.1.1 beta (SSE, ARM, MIPS optimisations)

* Sun Oct  5 2014 Peter Robinson <pbrobinson@fedoraproject.org> 1.1-5
- Install html docs in devel package

* Fri Oct  3 2014 Peter Robinson <pbrobinson@fedoraproject.org> 1.1-4
- Build developer docs

* Sun Aug 17 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Fri Dec  6 2013 Peter Robinson <pbrobinson@fedoraproject.org> 1.1-1
- 1.1 release

* Tue Dec  3 2013 Peter Robinson <pbrobinson@fedoraproject.org> 1.1-0.3rc3
- Update to 1.1-rc3

* Thu Nov 28 2013 Peter Robinson <pbrobinson@fedoraproject.org> 1.1-0.2rc2
- Update to 1.1-rc2

* Tue Nov 26 2013 Peter Robinson <pbrobinson@fedoraproject.org> 1.1-0.1rc
- Update to 1.1-rc

* Sat Aug 03 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Sun Jul 14 2013 Peter Robinson <pbrobinson@fedoraproject.org> 1.0.3-1
- 1.0.3 release

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.2-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Thu Jan 10 2013 Peter Robinson <pbrobinson@fedoraproject.org> 1.0.2-2
- Enable extra custom modes API

* Thu Dec  6 2012 Peter Robinson <pbrobinson@fedoraproject.org> 1.0.2-1
- Official 1.0.2 release

* Wed Sep 12 2012 Peter Robinson <pbrobinson@fedoraproject.org> - 1.0.1-1
- Official 1.0.1 release now rfc6716 is stable

* Tue Sep  4 2012 Peter Robinson <pbrobinson@fedoraproject.org> - 1.0.1rc3-0.1
- Update to 1.0.1rc3

* Thu Aug  9 2012 Peter Robinson <pbrobinson@fedoraproject.org> - 1.0.0rc1-0.1
- Update to 1.0.0rc1

* Fri Jul 20 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.14-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Sun May 27 2012 Peter Robinson <pbrobinson@fedoraproject.org> - 0.9.14-1
- Update to 0.9.14

* Sat May 12 2012 Peter Robinson <pbrobinson@fedoraproject.org> - 0.9.10-2
- Add make check - fixes RHBZ # 821128

* Fri Apr 27 2012 Peter Robinson <pbrobinson@fedoraproject.org> - 0.9.10-1
- Update to 0.9.10

* Fri Jan 13 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.8-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Tue Nov  8 2011 Peter Robinson <pbrobinson@fedoraproject.org> 0.9.8-1
- Update to 0.9.8

* Mon Oct 10 2011 Peter Robinson <pbrobinson@fedoraproject.org> 0.9.6-1
- Initial packaging
